<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\GradeController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\OptionController;
use App\Http\Controllers\LevelOptionController;
use App\Http\Controllers\AcademicYearController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\DepartmentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [UserController::class, 'login'])->name('login');

Route::middleware(['auth:api'])->group(function () {

    //Route::get('statisticsForDashboard', [DashboardController::class, 'statistics']);

    Route::resource('academic_years', AcademicYearController::class)->except(['show', 'edit', 'create']);
    Route::post('academic_years/enable/{id}', [AcademicYearController::class, 'undelete']);
    Route::post('academic_years/force_delete/{id}', [AcademicYearController::class, 'forceDelete']);
    Route::get('academic_years/paginate/{per_page}', [AcademicYearController::class, 'indexPaginate']);

    Route::resource('departments', DepartmentController::class)->except(['show', 'edit', 'create']);
    Route::post('departments/enable/{id}', [DepartmentController::class, 'undelete']);
    Route::post('departments/force_delete/{id}', [DepartmentController::class, 'forceDelete']);
    Route::get('departments/paginate/{per_page}', [DepartmentController::class, 'indexPaginate']);
    Route::get('departments/paginate/{per_page}/searchText/{search_text}', [DepartmentController::class, 'indexSearchPaginate']);

    Route::resource('falcuties', FacultyController::class)->except(['show', 'edit', 'create']);
    Route::post('falcuties/enable/{id}', [FacultyController::class, 'undelete']);
    Route::post('falcuties/force_delete/{id}', [FacultyController::class, 'forceDelete']);
    Route::get('falcuties/paginate/{per_page}', [FacultyController::class, 'indexPaginate']);
    Route::get('falcuties/options/paginate/{per_page}', [FacultyController::class, 'indexPaginateByOptions']);

    Route::resource('levels', LevelController::class)->except(['show', 'edit', 'create']);
    Route::post('levels/enable/{id}', [LevelController::class, 'undelete']);
    Route::post('levels/force_delete/{id}', [LevelController::class, 'forceDelete']);
    Route::get('levels/paginate/{per_page}', [LevelController::class, 'indexPaginate']);
    Route::get('levels/options/paginate/{per_page}', [LevelController::class, 'indexPaginateByOptions']);

    Route::resource('options', OptionController::class)->except(['show', 'edit', 'create']);
    Route::post('options/enable/{id}', [OptionController::class, 'undelete']);
    Route::post('options/force_delete/{id}', [OptionController::class, 'forceDelete']);
    Route::get('options/paginate/{per_page}', [OptionController::class, 'indexPaginate']);
    Route::get('options/paginate/{per_page}/searchText/{search_text}', [OptionController::class, 'indexSearchPaginate']);

    Route::resource('levels_options', LevelOptionController::class)->except(['show', 'edit', 'create']);
    Route::post('levels_options/enable/{id}', [LevelOptionController::class, 'undelete']);
    Route::post('levels_options/force_delete/{id}', [LevelOptionController::class, 'forceDelete']);
    Route::get('levels_options/paginate/{per_page}', [LevelOptionController::class, 'indexPaginate']);

    Route::resource('students', StudentController::class)->except(['show', 'edit', 'create']);
    Route::post('students/enable/{id}', [StudentController::class, 'undelete']);
    Route::post('students/force_delete/{id}', [StudentController::class, 'forceDelete']);
    Route::get('students/paginate/{per_page}', [StudentController::class, 'indexPaginate']);
    Route::get('students/paginate/{per_page}/searchText/{search_text}', [StudentController::class, 'indexSearchPaginate']);

    Route::resource('teachers', TeacherController::class)->except(['show', 'edit', 'create']);
    Route::post('teachers/enable/{id}', [TeacherController::class, 'undelete']);
    Route::post('teachers/force_delete/{id}', [TeacherController::class, 'forceDelete']);
    Route::get('teachers/paginate/{per_page}', [TeacherController::class, 'indexPaginate']);
    Route::get('teachers/paginate/{per_page}/searchText/{search_text}', [TeacherController::class, 'indexSearchPaginate']);

    Route::resource('units', UnitController::class)->except(['show', 'edit', 'create']);
    Route::post('units/enable/{id}', [UnitController::class, 'undelete']);
    Route::post('units/force_delete/{id}', [UnitController::class, 'forceDelete']);
    Route::get('units/paginate/{per_page}', [UnitController::class, 'indexPaginate']);
    Route::get('units/paginate/{per_page}/searchText/{search_text}', [UnitController::class, 'indexSearchPaginate']);

    Route::resource('users', UserController::class)->except(['show', 'edit', 'create']);
    Route::post('users/enable/{id}', [UserController::class, 'undelete']);
    Route::post('users/force_delete/{id}', [UserController::class, 'forceDelete']);
    Route::get('users/paginate/{per_page}', [UserController::class, 'indexPaginate']);
    Route::get('users/paginate/{per_page}/searchText/{search_text}', [UserController::class, 'indexSearchPaginate']);

    Route::resource('roles', RoleController::class)->only(['index']);
    Route::resource('grades', GradeController::class)->only(['index']);

    Route::post('logout', [UserController::class, 'logout']);

});
