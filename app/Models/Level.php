<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Level extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function options()
    {
        return $this->belongsToMany(Option::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
