<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Department;
use App\Models\Level;
use App\Models\Option;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $students = Student::all()->sortBy('name');
            $students->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->department = $item->department;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $teachers));
        }else{
            $students = Student::where('user_id', $logged_user->id)->get()->sortBy('name');
            return response()->json(new RESTResponse(200, "OK", $students));
        }
    }

    public function indexSearch($search_text){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $students = Student::withTrashed()
                                ->where('name', 'like', '%' . $search_text . '%')
                                ->get()->sortBy('name');
            $students->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->level = $item->level;
                $item->code = $item->level->code;
                $item->option = $item->option;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $teachers));
        }else{
            $students = Student::where('user_id', $logged_user->id)
                                ->where('name', 'like', '%' . $search_text . '%')
                                ->withTrashed()
                                ->get()->sortBy('name');
            $student->transform(function ($item, $key) {
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->level = $item->level;
                $item->code = $item->level->code;
                $item->option = $item->option;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $teachers));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $students = Student::withTrashed()->paginate($per_page);
            $students->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->level = $item->level;
                $item->code = $item->level->code;
                $item->option = $item->option;
                $item->code = $item->option->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                ->json(new RESTPaginateResponse($students->currentPage(), $students->items(), $students->url(1),
                $students->lastPage(), $students->url($students->lastPage()), $students->nextPageUrl(),
                $students->perPage(), $students->previousPageUrl(), $students->count(), $students->total()));
            }else{
                $students = Teacher::where('user_id', $logged_user->id)
                                ->withTrashed()->paginate($per_page);
            $students->transform(function ($item, $key) {
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->level = $item->level;
                $item->code = $item->level->code;
                $item->option = $item->option;
                $item->code = $item->option->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                ->json(new RESTPaginateResponse($students->currentPage(), $students->items(), $students->url(1),
                $students->lastPage(), $students->url($students->lastPage()), $students->nextPageUrl(),
                $students->perPage(), $students->previousPageUrl(), $students->count(), $students->total()));
            }
    }

	/**
     * Display a listing of the resource using search_text by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSearchPaginate($per_page = 15, $search_text=""){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $students = Student::withTrashed()->where('name', 'like', '%' . $search_text . '%')->paginate($per_page);
            $students->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->level = $item->level;
                $item->code = $item->level->code;
                $item->option = $item->option;
                $item->code = $item->option->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($students->currentPage(), $students->items(), $students->url(1),
                    $students->lastPage(), $students->url($students->lastPage()), $students->nextPageUrl(),
                    $students->perPage(), $students->previousPageUrl(), $students->count(), $students->total()));
        }else{
            $students = Student::where('user_id', $logged_user->id)
                                ->where('name', 'like', '%' . $search_text . '%')
                                ->withTrashed()->paginate($per_page);
            $students->transform(function ($item, $key) {
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->level = $item->level;
                $item->code = $item->level->code;
                $item->option = $item->option;
                $item->code = $item->option->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($students->currentPage(), $students->items(), $students->url(1),
                    $students->lastPage(), $students->url($students->lastPage()), $students->nextPageUrl(),
                    $students->perPage(), $students->previousPageUrl(), $students->count(), $students->total()));
        }
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
            $student = new Student;
            $student->name = $request->input('name');
            $student->email = $request->input('email');
            if(substr($request->input('matricule'), 0, 3) == "16F")
                $student->matricule = $request->input('matricule');
            else if($request->input('matricule') != "")
                $student->matricule = "16F".$request->input('matricule');
            else
                $student->matricule = $request->input('matricule');
            //$student->matricule = $request->input('matricule');
            $student->department()->associate(Department::find($request->input('department_id')));
            $student->level()->associate(Level::find($request->input('level_id')));
            $student->option()->associate(Option::find($request->input('option_id')));
            $student->user()->associate(User::find($logged_user->id));
            $student->save();
            return response()->json(new RESTResponse(200, "OK", $teacher));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        $logged_user = Auth::user();
        if($student != null){
            $student->name = $request->input('name');
            $student->matricule = $request->input('matricule');
            $student->email = $request->input('email');
            if(substr($request->input('matricule'), 0, 3) == "16F")
                $student->matricule = $request->input('matricule');
            else if($request->input('matricule') != "")
                $student->matricule = "16F".$request->input('matricule');
            else
                $student->matricule = $request->input('matricule');
            //$student->matricule = $request->input('matricule');
            $student->department()->dissociate();
            $student->level()->dissociate();
            $student->option()->dissociate();
            $student->department()->associate(Department::find($request->input('department_id')));
            $student->level()->associate(Level::find($request->input('level_id')));
            $student->option()->associate(Option::find($request->input('option_id')));
            $student->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }
        else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student=Student::find($id);
        $logged_user = Auth::user();
        if($student != null){
            $student->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $student=Student::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($student != null){
            $student->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $student=Student::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($student != null){
            $student->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
