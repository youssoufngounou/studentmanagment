<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use App\Models\Department;
use App\Models\Grade;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $teachers = Teacher::all()->sortBy('name');
            $teachers->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $teachers));
        }else{
            $teachers = Teacher::where('user_id', $logged_user->id)->get()->sortBy('name');
            $teachers->transform(function ($item, $key) {
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $teachers));
        }
    }

    public function indexSearch($search_text){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $teachers = Teacher::withTrashed()
                                ->where('name', 'like', '%' . $search_text . '%')
                                ->get()->sortBy('name');
            $teachers->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $teachers));
        }else{
            $teachers = Teacher::where('user_id', $logged_user->id)
                                ->where('name', 'like', '%' . $search_text . '%')
                                ->withTrashed()
                                ->get()->sortBy('name');
            $teachers->transform(function ($item, $key) {
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $teachers));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $teachers = Teacher::withTrashed()->paginate($per_page);
            $teachers->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($teachers->currentPage(), $teachers->items(), $teachers->url(1), $teachers->lastPage(), $teachers->url($teachers->lastPage()), $teachers->nextPageUrl(), $teachers->perPage(), $teachers->previousPageUrl(), $teachers->count(), $teachers->total()));
        }else{
            $teachers = Teacher::where('user_id', $logged_user->id)
                                ->withTrashed()->paginate($per_page);
            $teachers->transform(function ($item, $key) {
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($teachers->currentPage(), $teachers->items(), $teachers->url(1), $teachers->lastPage(), $teachers->url($teachers->lastPage()), $teachers->nextPageUrl(), $teachers->perPage(), $teachers->previousPageUrl(), $teachers->count(), $teachers->total()));
        }
    }

	/**
     * Display a listing of the resource using search_text by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSearchPaginate($per_page = 15, $search_text=""){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $teachers = Teacher::withTrashed()->where('name', 'like', '%' . $search_text . '%')->paginate($per_page);
            $teachers->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($teachers->currentPage(), $teachers->items(), $teachers->url(1), $teachers->lastPage(), $teachers->url($teachers->lastPage()), $teachers->nextPageUrl(), $teachers->perPage(), $teachers->previousPageUrl(), $teachers->count(), $teachers->total()));
        }else{
            $teachers = Teacher::where('user_id', $logged_user->id)
                                ->where('name', 'like', '%' . $search_text . '%')
                                ->withTrashed()->paginate($per_page);
            $teachers->transform(function ($item, $key) {
                $item->grade = $item->grade;
                $item->grade_description = $item->grade->description;
                $item->department = $item->department;
                $item->department_code = $item->department->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($teachers->currentPage(), $teachers->items(), $teachers->url(1), $teachers->lastPage(), $teachers->url($teachers->lastPage()), $teachers->nextPageUrl(), $teachers->perPage(), $teachers->previousPageUrl(), $teachers->count(), $teachers->total()));
        }
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        // if( $request->input('email') != "" && !Teacher::where('email', $request->input('email'))->exists() && $request->input('telephone') != "" && !Teacher::where('telephone', "237".$request->input('telephone'))->exists()){
            $teacher = new Teacher;
            $teacher->name = $request->input('name');
            $teacher->sex = $request->input('sex');
            $teacher->email = $request->input('email');
            if(substr($request->input('telephone'), 0, 3) == "237")
                $teacher->telephone = $request->input('telephone');
            else if($request->input('telephone') != "")
                $teacher->telephone = "237".$request->input('telephone');
            else
                $teacher->telephone = $request->input('telephone');
            $teacher->matricule = $request->input('matricule');
            $teacher->department()->associate(Department::find($request->input('department_id')));
            $teacher->grade()->associate(Grade::find($request->input('grade_id')));
            $teacher->user()->associate(User::find($logged_user->id));
            $teacher->save();
            return response()->json(new RESTResponse(200, "OK", $teacher));
        // }else if($request->input('email') != "" && Teacher::where('email', $request->input('email'))->exists())
        //     return response()->json(new RESTResponse(300, "Un utilisateur possédant l'adresse e-mail: ".$request->input('email')." existe déjà dans votre base de données !", null));
        // else
        //     return response()->json(new RESTResponse(300, "Un utilisateur possédant le numéro de téléphone: ".$request->input('telephone')." existe déjà dans votre base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = Teacher::find($id);
        $logged_user = Auth::user();
        // if($logged_user->role_id == 1 && $teacher != null && !Teacher::where('email', $request->input('email'))->exists() && !Teacher::where('telephone', "237".$request->input('telephone'))->exists()){
        if($teacher != null){
            $teacher->name = $request->input('name');
            $teacher->sex = $request->input('sex');
            $teacher->email = $request->input('email');
            if(substr($request->input('telephone'), 0, 3) == "237")
                $teacher->telephone = $request->input('telephone');
            else if($request->input('telephone') != "")
                $teacher->telephone = "237".$request->input('telephone');
            else
                $teacher->telephone = $request->input('telephone');
            $teacher->matricule = $request->input('matricule');
            $teacher->department()->dissociate();
            $teacher->grade()->dissociate();
            $teacher->department()->associate(Department::find($request->input('department_id')));
            $teacher->grade()->associate(Grade::find($request->input('grade_id')));
            $teacher->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }
        // else if(Teacher::where('email', $request->input('email'))->exists())
        //     return response()->json(new RESTResponse(300, "Un utilisateur possédant l'adresse e-mail: ".$request->input('email')." existe déjà dans votre base de données !", null));
        // else if(Teacher::where('telephone', "237".$request->input('telephone'))->exists())
        //     return response()->json(new RESTResponse(300, "Un utilisateur possédant le numéro de téléphone: ".$request->input('telephone')." existe déjà dans votre base de données !", null));
        // else if($logged_user->role_id != 1)
        //     return response()->json(new RESTResponse(403, "Vous n'êtes pas autorisé à effectuer cette opération !", null));
        else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher=Teacher::find($id);
        $logged_user = Auth::user();
        if($teacher != null){
            $teacher->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $teacher=Teacher::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($teacher != null){
            $teacher->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $teacher=Teacher::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($teacher != null){
            $teacher->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
