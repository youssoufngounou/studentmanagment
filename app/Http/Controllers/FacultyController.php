<?php

namespace App\Http\Controllers;

use App\Models\Faculty;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $faculties = Faculty::all()->sortBy('name');
            $faculties->transform(function ($item, $key) {
                $item->department = $item->department;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $faculties));
        }else{
            $faculties = Faculty::where('department_id', $logged_user->id)->get()->sortBy('code');
            return response()->json(new RESTResponse(200, "OK", $faculties));
        }
    }

    public function indexSearch($search_text){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $faculties = Faculty::withTrashed()
                            ->where('code', 'like', '%' . $search_text . '%')
                            ->get()->sortBy('code');
            $faculties->transform(function ($item, $key) {
                $item->department = $item->department;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $faculties));
        }else{
            $faculties = Faculty::where('user_id', $logged_user->id)
                                    ->where('code', 'like', '%' . $search_text . '%')
                                    ->withTrashed()
                                    ->get()->sortBy('code');
            return response()->json(new RESTResponse(200, "OK", $faculties));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $faculties = Faculty::withTrashed()->paginate($per_page);
            $faculties->transform(function ($item, $key) {
                $item->department = $item->department;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($faculties->currentPage(),
                    $faculties->items(), $faculties->url(1), $faculties->lastPage(),
                    $faculties->url($faculties->lastPage()), $faculties->nextPageUrl(),
                    $faculties->perPage(), $faculties->previousPageUrl(),
                    $faculties->count(), $faculties->total()));
        }else{
            $faculties = Faculty::where('department_id', $logged_user->id)
                                    ->withTrashed()->paginate($per_page);
            $faculties->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($faculties->currentPage(),
                    $faculties->items(), $faculties->url(1), $faculties->lastPage(),
                    $faculties->url($faculties->lastPage()), $faculties->nextPageUrl(),
                    $faculties->perPage(), $faculties->previousPageUrl(),
                    $faculties->count(), $faculties->total()));
        }
    }

	/**
     * Display a listing of the resource using search_text by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSearchPaginate($per_page = 15, $search_text=""){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $faculties = Faculty::withTrashed()->where('code', 'like', '%' . $search_text . '%')->paginate($per_page);
            $faculties->transform(function ($item, $key) {
                $item->department = $item->department;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($faculties->currentPage(),
                    $faculties->items(), $faculties->url(1), $faculties->lastPage(),
                    $faculties->url($faculties->lastPage()), $faculties->nextPageUrl(),
                    $faculties->perPage(), $faculties->previousPageUrl(),
                    $faculties->count(), $faculties->total()));
        }else{
            $faculties = Faculty::where('department_id', $logged_user->id)
                                        ->where('code', 'like', '%' . $search_text . '%')
                                        ->withTrashed()
                                        ->paginate($per_page);
            $faculties->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($faculties->currentPage(),
                    $faculties->items(), $faculties->url(1), $faculties->lastPage(),
                    $faculties->url($faculties->lastPage()), $faculties->nextPageUrl(),
                    $faculties->perPage(), $faculties->previousPageUrl(),
                    $faculties->count(), $faculties->total()));
        }
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        if(!Faculty::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
            $faculties = new Department;
            $faculties->code = $request->input('name');
            $faculties->name = $request->input('code');
            $faculties->user()->associate(User::find($logged_user->id));
            $faculties->save();
            return response()->json(new RESTResponse(200, "OK", $department));
        }else
            return response()->json(new RESTResponse(300, "Une faculté possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faculty = Faculty::find($id);
        $logged_user = Auth::user();
        if($faculty != null && !Faculty::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
            $faculty->code = $request->input('name');
            $faculty->name = $request->input('code');
            $faculty->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }else if($faculty == null)
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        else
            return response()->json(new RESTResponse(300, "Un département possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faculty=Faculty::find($id);
        $logged_user = Auth::user();
        if($faculty != null){
            $faculty->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $faculty=Faculty::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($faculty != null){
            $faculty->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $faculty=Faculty::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($faculty != null){
            $faculty->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
