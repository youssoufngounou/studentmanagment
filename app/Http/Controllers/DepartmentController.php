<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $departments = Department::all()->sortBy('department_code');
            $departments->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $departments));
        }else{
            $departments = Department::where('user_id', $logged_user->id)->get()->sortBy('department_code');
            return response()->json(new RESTResponse(200, "OK", $departments));
        }
    }

    public function indexSearch($search_text){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $departments = Department::withTrashed()
                            ->where('department_code', 'like', '%' . $search_text . '%')
                            ->get()->sortBy('department_code');
            $departments->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $departments));
        }else{
            $departments = Department::where('user_id', $logged_user->id)
                                    ->where('department_code', 'like', '%' . $search_text . '%')
                                    ->withTrashed()
                                    ->get()->sortBy('department_code');
            return response()->json(new RESTResponse(200, "OK", $departments));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $departments = Department::withTrashed()->paginate($per_page);
            $departments->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($departments->currentPage(), $departments->items(), $departments->url(1), $departments->lastPage(), $departments->url($departments->lastPage()), $departments->nextPageUrl(), $departments->perPage(), $departments->previousPageUrl(), $departments->count(), $departments->total()));
        }else{
            $departments = Department::where('user_id', $logged_user->id)
                                    ->withTrashed()->paginate($per_page);
            $departments->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($departments->currentPage(), $departments->items(), $departments->url(1), $departments->lastPage(), $departments->url($departments->lastPage()), $departments->nextPageUrl(), $departments->perPage(), $departments->previousPageUrl(), $departments->count(), $departments->total()));
        }
    }

	/**
     * Display a listing of the resource using search_text by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSearchPaginate($per_page = 15, $search_text=""){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $departments = Department::withTrashed()->where('department_code', 'like', '%' . $search_text . '%')->paginate($per_page);
            $departments->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($departments->currentPage(), $departments->items(), $departments->url(1), $departments->lastPage(), $departments->url($departments->lastPage()), $departments->nextPageUrl(), $departments->perPage(), $departments->previousPageUrl(), $departments->count(), $departments->total()));
        }else{
            $departments = Department::where('user_id', $logged_user->id)
                                        ->where('department_code', 'like', '%' . $search_text . '%')
                                        ->withTrashed()
                                        ->paginate($per_page);
            $departments->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($departments->currentPage(), $departments->items(), $departments->url(1), $departments->lastPage(), $departments->url($departments->lastPage()), $departments->nextPageUrl(), $departments->perPage(), $departments->previousPageUrl(), $departments->count(), $departments->total()));
        }
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        if(!Department::where('user_id', $logged_user->id)->where('department_code', $request->input('department_code'))->exists()){
            $department = new Department;
            $department->code = $request->input('department_name');
            $department->name = $request->input('department_code');
            $department->user()->associate(User::find($logged_user->id));
            $department->save();
            return response()->json(new RESTResponse(200, "OK", $department));
        }else
            return response()->json(new RESTResponse(300, "Un département possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);
        $logged_user = Auth::user();
        if($department != null && !Department::where('user_id', $logged_user->id)->where('department_code', $request->input('department_code'))->exists()){
            $department->code = $request->input('department_name');
            $department->name = $request->input('department_code');
            $department->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }else if($department == null)
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        else
            return response()->json(new RESTResponse(300, "Un département possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department=Department::find($id);
        $logged_user = Auth::user();
        if($department != null){
            $department->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $department=Department::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($department != null){
            $department->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $department=Department::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($department != null){
            $department->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
