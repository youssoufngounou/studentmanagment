<?php

namespace App\Http\Controllers;

use App\Models\Option;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $options = Option::all()->sortBy('name');
            $options->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $options));
        }else{
            $options = Option::where('user_id', $logged_user->id)->get()->sortBy('name');
            return response()->json(new RESTResponse(200, "OK", $options));
        }
    }

    public function indexSearch($search_text){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $options = Option::withTrashed()
                            ->where('name', 'like', '%' . $search_text . '%')
                            ->get()->sortBy('name');
            $options->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $options));
        }else{
            $options = Option::where('user_id', $logged_user->id)
                            ->where('name', 'like', '%' . $search_text . '%')
                            ->withTrashed()
                            ->get()->sortBy('name');
            return response()->json(new RESTResponse(200, "OK", $options));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $options = Option::withTrashed()->paginate($per_page);
            $options->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($options->currentPage(), $options->items(), $options->url(1), $options->lastPage(), $options->url($options->lastPage()), $options->nextPageUrl(), $options->perPage(), $options->previousPageUrl(), $options->count(), $options->total()));
        }else{
            $options = Option::where('user_id', $logged_user->id)
                                ->withTrashed()->paginate($per_page);
            $options->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($options->currentPage(), $options->items(), $options->url(1), $options->lastPage(), $options->url($options->lastPage()), $options->nextPageUrl(), $options->perPage(), $options->previousPageUrl(), $options->count(), $options->total()));
        }
    }

	/**
     * Display a listing of the resource using search_text by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSearchPaginate($per_page = 15, $search_text=""){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $options = Option::withTrashed()->where('name', 'like', '%' . $search_text . '%')->paginate($per_page);
            $options->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($options->currentPage(), $options->items(), $options->url(1), $options->lastPage(), $options->url($options->lastPage()), $options->nextPageUrl(), $options->perPage(), $options->previousPageUrl(), $options->count(), $options->total()));
        }else{
            $options = Option::where('user_id', $logged_user->id)
                                ->where('name', 'like', '%' . $search_text . '%')
                                ->withTrashed()->paginate($per_page);
            $options->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($options->currentPage(), $options->items(), $options->url(1), $options->lastPage(), $options->url($options->lastPage()), $options->nextPageUrl(), $options->perPage(), $options->previousPageUrl(), $options->count(), $options->total()));
        }
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        if(!Option::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
            $option = new Option;
            $option->code = $request->input('code');
            $option->name = $request->input('name');
            $option->user()->associate(User::find($logged_user->id));
            $option->save();
            return response()->json(new RESTResponse(200, "OK", $option));
        }else
            return response()->json(new RESTResponse(300, "Une option possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $option = Option::find($id);
        $logged_user = Auth::user();
        if($option != null && !Option::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
            $option->code = $request->input('code');
            $option->name = $request->input('name');
            $option->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }else if($option == null)
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        else
            return response()->json(new RESTResponse(300, "Une option possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $option=Option::find($id);
        $logged_user = Auth::user();
        if($option != null){
            $option->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $option=Option::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($option != null){
            $option->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $level=Level::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($level != null){
            $level->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
