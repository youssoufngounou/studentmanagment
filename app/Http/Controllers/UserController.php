<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client as OClient;
use Illuminate\Support\Facades\Route;
use DB;
use App\Http\ResponsesModels\UserResponse;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        //if($logged_user->role_id == 1){
            $users = User::all()->sortBy('name');
            $users->transform(function ($item, $key) {
                $item->post = $item->role->description;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $users));
            //return response()->json([$users, 200]);
        /*} else {
            return response()->json(new RESTResponse(403, "Vous n'êtes pas autorisé à effectuer cette tâche !", null));
        }*/
    }

    public function indexSearch($search_text){
        $logged_user = Auth::user();
        $users;
        if($logged_user->role_id == 1){
            $users = User::withTrashed()
                            ->where('name', 'like', '%' . $search_text . '%')
                            ->get()->sortBy('name');
            $users->transform(function ($item, $key) {
                $item->post = $item->role->description;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $users));
        } else {
            return response()->json(new RESTResponse(403, "Vous n'êtes pas autorisé à effectuer cela !", null));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $users = User::withTrashed()->paginate($per_page);
            $users->transform(function ($item, $key) {
                $item->post = $item->role->description;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($users->currentPage(), $users->items(), $users->url(1), $users->lastPage(), $users->url($users->lastPage()), $users->nextPageUrl(), $users->perPage(), $users->previousPageUrl(), $users->count(), $users->total()));
        } else {
            return response()->json(new RESTResponse(403, "Vous n'avez pas les autorisation pour effectuer cette opération !", null));
        }
    }

    /**
     * Display a listing of the resource using search_text by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSearchPaginate($per_page = 15, $search_text=""){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $users = User::withTrashed()->where('name', 'like', '%' . $search_text . '%')->paginate($per_page);
            $users->transform(function ($item, $key) {
                $item->post = $item->role->description;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($users->currentPage(), $users->items(), $users->url(1), $users->lastPage(), $users->url($users->lastPage()), $users->nextPageUrl(), $users->perPage(), $users->previousPageUrl(), $users->count(), $users->total()));
        } else {
            return response()->json(new RESTResponse(403, "Vous nêtes pas autorisé à effectuer cette opération !", null));
        }
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        if(!User::where('email', $request->input('email'))->exists() && !User::where('matricule', "16F".$request->input('matricule'))->exists()){
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if(substr($request->input('matricule'), 0, 3) == "16F") {
                $user->matricule = $request->input('matricule');
            } else if($request->input('matricule') != "") {
                $user->matricule = "16F".$request->input('matricule');
            } else {
                $user->telephone = $request->input('matricule');
            }
            $user->password = bcrypt($request->input('password'));
            $user->role()->associate(Role::find($request->input('role')));
            $user->save();
            return response()->json(new RESTResponse(200, "OK", $user));
        } else if(User::where('email', $request->input('email'))->exists()) {
            return response()->json(new RESTResponse(300, "Un utilisateur possédant l'adresse e-mail: ".$request->input('email')." existe déjà dans votre base de données !", null));
        } else {
            return response()->json(new RESTResponse(300, "Un utilisateur possédant ce matricule: ".$request->input('matricule')." existe déjà dans votre base de données !", null));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $logged_user = Auth::user();
        if($user != null && $logged_user->role_id == 1){
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if(substr($request->input('matricule'), 0, 3) == "16F") {
                $user->matricule = $request->input('matricule');
            } else if($request->input('matricule') != "") {
                $user->matricule = "16F".$request->input('matricule');
            } else {
                $user->matricule = $request->input('matricule');
            }
            if($request->input('password')!="") {
                $user->password = bcrypt($request->input('password'));
            }
            $user->role()->dissociate();;
            $user->role()->associate(Role::find($request->input('role')));
            $user->save();
            return response()->json(new RESTResponse(200, "OK", null));
        } else if($logged_user->role_id != 1) {
            return response()->json(new RESTResponse(403, "Vous n'avez pas les autorisations nécessaires !", null));
        } else {
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $logged_user = Auth::user();
        if($user != null && $logged_user->role_id == 1){
            $user->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        } else if ($logged_user->role_id != 1) {
            return response()->json(new RESTResponse(403, "Vous nêtes pas autorisé à effectuer cette opération !", null));
        } else {
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
        }
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $user=User::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($user != null && $logged_user->role_id == 1){
            $user->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        } else if($logged_user->role_id != 1) {
            return response()->json(new RESTResponse(403, "Vous n'êtes pas autorisé à effectuer cette opération !", null));
        } else {
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
        }
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $user=User::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($user != null && $logged_user->role_id == 1){
            $user->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else if($logged_user->role_id != 1) {
            return response()->json(new RESTResponse(403, "Vous n'êtes pas autorisé à effectuer cette opération !", null));
        } else {
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
        }
    }

    protected function credentials(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');
        if(is_numeric($username)){
            return ['matricule' => $request->get('username'), 'password' => $password];
        }
        else if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return ['email' => $username, 'password' => $password];
        }
    }

    /**
     * Login User into Application.
     *
     * @param  Request  $request
     * @return redirect()
     */
    public function login(Request $request){
        $username = $request->get('username');
        $password = $request->get('password');
        $credentials;
        if(is_numeric($request->get('username'))){
            $username = "16F".$username;
            $credentials = ['matricule' => $username, 'password' => $password];
        } else if (filter_var($request->get('username'), FILTER_VALIDATE_EMAIL)) {
            $credentials = ['email' => $username, 'password' => $password];
        } else {
            return response()->json(['code' => 401], 200);
        }
        if (Auth::attempt($credentials)){
            $user = Auth::user();
            if($user->deleted_at == null){
                $oClient = DB::table('oauth_clients')->where('password_client', 1)->first();
                $data = [
                    'username' => $username,
                    'password' => $password,
                    'client_id' => $oClient->id,
                    'client_secret' => $oClient->secret,
                    'grant_type' => 'password',
                ];
                $request = app('request')->create('/oauth/token', 'POST', $data);
                $response = app('router')->prepareResponse($request, app()->handle($request));
                $result = json_decode((string) $response->content(), true);
                $user->role = $user->role;
                $user->tokens = $result;
                $tabdata=array('code' => 200, 'user' => $user);
                return response()->json($tabdata, 200);
            } else {
                return response()->json(['code' => 403], 200);
            }
        }
        else {
            return response()->json(['code' => 401], 200);
        }
    }

    /**
     * Log out current User connected.
     *
     * @return redirect()
     */
    public function logout(){
        $logged_user = Auth::user();
        if($logged_user != null){
            $tokenRepository = app('Laravel\Passport\TokenRepository');
            $refreshTokenRepository = app('Laravel\Passport\RefreshTokenRepository');
            DB::table('oauth_access_tokens')
                ->where('user_id', $logged_user->id)
                ->get()
                ->each(function ($item, $key) use($tokenRepository, $refreshTokenRepository) {
                    $tokenRepository->revokeAccessToken($item->id);
                    $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($item->id);
                });
            return response()->json(['code' => 200], 200);
        } else {
            return response()->json(['code' => 401], 200);
        }
    }
}
