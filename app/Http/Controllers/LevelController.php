<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $levels = Level::all();
            $levels->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $levels));
        }else{
            $levels = Level::where('user_id', $logged_user->id)->get();
            return response()->json(new RESTResponse(200, "OK", $levels));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $levels = Level::withTrashed()->paginate($per_page);
            $levels->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($levels->currentPage(), $levels->items(), $levels->url(1), $levels->lastPage(), $levels->url($levels->lastPage()), $levels->nextPageUrl(), $levels->perPage(), $levels->previousPageUrl(), $levels->count(), $levels->total()));
        }else{
            $levels = Level::where('user_id', $logged_user->id)->withTrashed()->paginate($per_page);
            $levels->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($levels->currentPage(), $levels->items(), $levels->url(1), $levels->lastPage(), $levels->url($levels->lastPage()), $levels->nextPageUrl(), $levels->perPage(), $levels->previousPageUrl(), $levels->count(), $levels->total()));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginateByOptions($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $levels = Level::withTrashed()->paginate($per_page);
            $levels->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->options = $item->options;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($levels->currentPage(), $levels->items(), $levels->url(1), $levels->lastPage(), $levels->url($levels->lastPage()), $levels->nextPageUrl(), $levels->perPage(), $levels->previousPageUrl(), $levels->count(), $levels->total()));
        }else{
            $levels = Level::where('user_id', $logged_user->id)->withTrashed()->paginate($per_page);
            $levels->transform(function ($item, $key) {
                $item->options = $item->options;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($levels->currentPage(), $levels->items(), $levels->url(1), $levels->lastPage(), $levels->url($levels->lastPage()), $levels->nextPageUrl(), $levels->perPage(), $levels->previousPageUrl(), $levels->count(), $levels->total()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        if(!Level::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
            $level = new Level;
            $level->code = $request->input('code');
            $level->user()->associate(User::find($logged_user->id));
            $level->save();
            return response()->json(new RESTResponse(200, "OK", $level));
        }else
            return response()->json(new RESTResponse(300, "Un niveau possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $level = Level::find($id);
        $logged_user = Auth::user();
        if($level != null && !Level::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
            $level->code = $request->input('code');
            $level->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }else if($level == null)
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        else
            return response()->json(new RESTResponse(300, "Un niveau possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level=Level::find($id);
        $logged_user = Auth::user();
        if($level != null){
            $level->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $level=Level::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($level != null){
            $level->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $level=Level::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($level != null){
            $level->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
