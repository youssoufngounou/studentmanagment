<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use App\Models\Department;
use App\Models\Level;
use App\Models\User;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $courses = Course::all()->sortBy('description');
            $courses->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $courses));
        }else{
            $courses = Course::where('user_id', $logged_user->id)->get()->sortBy('description');
            return response()->json(new RESTResponse(200, "OK", $courses));
        }
    }

    public function indexSearch($search_text){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $courses = Course::withTrashed()
                            ->where('description', 'like', '%' . $search_text . '%')
                            ->get()->sortBy('description');
            $courses->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $courses));
        }else{
            $courses = Course::where('user_id', $logged_user->id)
                            ->where('description', 'like', '%' . $search_text . '%')
                            ->withTrashed()
                            ->get()->sortBy('description');
            return response()->json(new RESTResponse(200, "OK", $courses));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $courses = Course::withTrashed()->paginate($per_page);
            $courses->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->department = $item->department;
                $item->department_code = $item->department->department_code;
                $item->level = $item->level;
                $item->level_name = $item->level->name;
                $item->teacher = $item->teacher;
                $item->teacher_matricule = $item->teacher->matricule;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($courses->currentPage(), $courses->items(), $courses->url(1), $courses->lastPage(), $courses->url($courses->lastPage()), $courses->nextPageUrl(), $courses->perPage(), $courses->previousPageUrl(), $courses->count(), $courses->total()));
        }else{
            $courses = Course::where('user_id', $logged_user->id)
                                ->withTrashed()->paginate($per_page);
            $courses->transform(function ($item, $key) {
                $item->department = $item->department;
                $item->department_code = $item->department->department_code;
                $item->level = $item->level;
                $item->level_name = $item->level->name;
                $item->teacher = $item->teacher;
                $item->teacher_matricule = $item->teacher->matricule;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($courses->currentPage(), $courses->items(), $courses->url(1), $courses->lastPage(), $courses->url($courses->lastPage()), $courses->nextPageUrl(), $courses->perPage(), $courses->previousPageUrl(), $courses->count(), $courses->total()));
        }
    }

	/**
     * Display a listing of the resource using search_text by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSearchPaginate($per_page = 15, $search_text=""){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $courses = Course::withTrashed()->where('description', 'like', '%' . $search_text . '%')->paginate($per_page);
            $courses->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->department = $item->department;
                $item->department_code = $item->department->department_code;
                $item->level = $item->level;
                $item->level_name = $item->level->name;
                $item->teacher = $item->teacher;
                $item->teacher_matricule = $item->teacher->matricule;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($courses->currentPage(), $courses->items(), $courses->url(1), $courses->lastPage(), $courses->url($courses->lastPage()), $courses->nextPageUrl(), $courses->perPage(), $courses->previousPageUrl(), $courses->count(), $courses->total()));
        }else{
            $courses = Course::where('user_id', $logged_user->id)
                            ->where('description', 'like', '%' . $search_text . '%')
                            ->withTrashed()->paginate($per_page);
            $courses->transform(function ($item, $key) {
                $item->department = $item->department;
                $item->department_code = $item->department->department_code;
                $item->level = $item->level;
                $item->level_name = $item->level->name;
                $item->teacher = $item->teacher;
                $item->teacher_matricule = $item->teacher->matricule;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                ->json(new RESTPaginateResponse($courses->currentPage(), $courses->items(), $courses->url(1), $courses->lastPage(), $courses->url($courses->lastPage()), $courses->nextPageUrl(), $courses->perPage(), $courses->previousPageUrl(), $courses->count(), $courses->total()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        // if(!Course::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
            $course = new Course;
            $course->code = $request->input('code');
            $course->code = $request->input('name');
            $course->description = $request->input('description');
            $course->code = $request->input('credit_number');
            $course->semester = $request->input('semester');
            $course->department()->associate(Department::find($request->input('department_id')));
            $course->level()->associate(Level::find($request->input('level_id')));
            $course->teacher()->associate(Teacher::find($request->input('teacher_id')));
            $course->user()->associate(User::find($logged_user->id));
            $course->save();
            return response()->json(new RESTResponse(200, "OK", $course));
        // }else
        //     return response()->json(new RESTResponse(300, "Un cours possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::find($id);
        $logged_user = Auth::user();
        // if($course != null && !Course::where('user_id', $logged_user->id)->where('code', $request->input('code'))->exists()){
        if($course != null){
            $course->code = $request->input('code');
            $course->code = $request->input('name');
            $course->description = $request->input('description');
            $course->code = $request->input('credit_number');
            $course->semester = $request->input('semester');
            $course->department()->associate(Department::find($request->input('department_id')));
            $course->level()->associate(Level::find($request->input('level_id')));
            $course->teacher()->associate(Teacher::find($request->input('teacher_id')));
            $course->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }
        else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        // else
        //     return response()->json(new RESTResponse(300, "Un cours possédant le code: ".$request->input('code')." existe déjà dans votre base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit=Unit::find($id);
        $logged_user = Auth::user();
        if($unit != null){
            $unit->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $unit=Unit::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($unit != null){
            $unit->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $unit=Unit::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($unit != null){
            $unit->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
