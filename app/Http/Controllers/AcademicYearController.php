<?php

namespace App\Http\Controllers;

use App\Models\academicYear;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;

class AcademicYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $academic_years = AcademicYear::all();
            $academic_years->transform(function ($item, $key) {
                $item->user = $item->user;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $academic_years));
        }else{
            $academic_years = AcademicYear::where('user_id', $logged_user->id)->get();
            return response()->json(new RESTResponse(200, "OK", $academic_years));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role_id == 1){
            $academic_years = AcademicYear::withTrashed()->paginate($per_page);
            $academic_years->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($academic_years->currentPage(), $academic_years->items(), $academic_years->url(1), $academic_years->lastPage(), $academic_years->url($academic_years->lastPage()), $academic_years->nextPageUrl(), $academic_years->perPage(), $academic_years->previousPageUrl(), $academic_years->count(), $academic_years->total()));
        }else{
            $academic_years = AcademicYear::where('user_id', $logged_user->id)
                                            ->withTrashed()->paginate($per_page);
            $academic_years->transform(function ($item, $key) {
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($academic_years->currentPage(), $academic_years->items(), $academic_years->url(1), $academic_years->lastPage(), $academic_years->url($academic_years->lastPage()), $academic_years->nextPageUrl(), $academic_years->perPage(), $academic_years->previousPageUrl(), $academic_years->count(), $academic_years->total()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        if(!AcademicYear::where('user_id', $logged_user->id)->where('year1', $request->input('year1'))->where('year2', $request->input('year2'))->exists()){
            $academic_year = new AcademicYear;
            $academic_year->year1 = $request->input('year1');
            $academic_year->year2 = $request->input('year2');
            $academic_year->user()->associate(User::find($logged_user->id));
            $academic_year->save();
            return response()->json(new RESTResponse(200, "OK", $academic_year));
        }else
            return response()->json(new RESTResponse(300, "Une année académique ".$request->input('year1')." - ".$request->input('year2')." existe déjà dans la base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $academic_year = AcademicYear::find($id);
        $logged_user = Auth::user();
        if($academic_year != null && !AcademicYear::where('user_id', $logged_user->id)->where('year1', $request->input('year1'))->where('year2', $request->input('year2'))->exists()){
            $academic_year->year1 = $request->input('year1');
            $academic_year->year2 = $request->input('year2');
            $academic_year->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }else if($academic_year == null)
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        else
            return response()->json(new RESTResponse(300, "Une année académique ".$request->input('year1')." - ".$request->input('year2')." existe déjà dans la base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $academic_year=AcademicYear::find($id);
        $logged_user = Auth::user();
        if($academic_year != null){
            $academic_year->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $academic_year=AcademicYear::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($academic_year != null){
            $academic_year->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $academic_year=AcademicYear::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($academic_year != null){
            $academic_year->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
