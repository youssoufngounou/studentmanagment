<?php

namespace App\Http\Controllers;

use App\Models\levelOption;
use App\Models\Level;
use App\Models\Option;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\ResponsesModels\RESTResponse;
use App\Http\ResponsesModels\RESTPaginateResponse;
use Illuminate\Support\Facades\Auth;

class LevelOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $levels_options = LevelOption::all();
            $levels_options->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->level = $item->level;
                $item->option = $item->option;
                $item->level_code = $item->level->code;
                $item->option_code = $item->option->code;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $levels_options));
        }else{
            $levels_options = LevelOption::where('user_id', $logged_user->id)->get();
            $levels_options->transform(function ($item, $key) {
                $item->level = $item->level;
                $item->option = $item->option;
                $item->level_code = $item->level->code;
                $item->option_code = $item->option->code;
                return $item;
            });
            return response()->json(new RESTResponse(200, "OK", $levels_options));
        }
    }

    /**
     * Display a listing of the resource by page.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPaginate($per_page = 15){
        $logged_user = Auth::user();
        if($logged_user->role->id == 1){
            $levels_options = LevelOption::withTrashed()->paginate($per_page);
            $levels_options->transform(function ($item, $key) {
                $item->user = $item->user;
                $item->level = $item->level;
                $item->option = $item->option;
                $item->level_code = $item->level->code;
                $item->option_code = $item->option->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($levels_options->currentPage(), $levels_options->items(), $levels_options->url(1), $levels_options->lastPage(), $levels_options->url($levels_options->lastPage()), $levels_options->nextPageUrl(), $levels_options->perPage(), $levels_options->previousPageUrl(), $levels_options->count(), $levels_options->total()));
        }else{
            $levels_options = LevelOption::where('user_id', $logged_user->id)->withTrashed()->paginate($per_page);
            $levels_options->transform(function ($item, $key) {
                $item->level = $item->level;
                $item->option = $item->option;
                $item->level_code = $item->level->code;
                $item->option_code = $item->option->code;
                $item->cree_le = $item->created_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->created_at));
                $item->modifie_le = $item->updated_at == null ? null : ''.date('d-m-Y à H:i:s', strtotime($item->updated_at));
                return $item;
            });
            return response()
                    ->json(new RESTPaginateResponse($levels_options->currentPage(), $levels_options->items(), $levels_options->url(1), $levels_options->lastPage(), $levels_options->url($levels_options->lastPage()), $levels_options->nextPageUrl(), $levels_options->perPage(), $levels_options->previousPageUrl(), $levels_options->count(), $levels_options->total()));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logged_user = Auth::user();
        if(!LevelOption::where('user_id', $logged_user->id)->where('level_id', $request->input('level_id'))->where('option_id', $request->input('option_id'))->exists()){
            $level_option = new LevelOption;
            $level_option->level()->associate(Level::find($request->input('level_id')));
            $level_option->option()->associate(Option::find($request->input('option_id')));
            $level_option->user()->associate(User::find($logged_user->id));
            $level_option->save();
            return response()->json(new RESTResponse(200, "OK", $level_option));
        }else
            return response()->json(new RESTResponse(300, $request->input('level_id')." : ".$request->input('option_id')." existe déjà dans votre base de données !", null));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $level_option = LevelOption::find($id);
        $logged_user = Auth::user();
        if($level_option != null && !LevelOption::where('user_id', $logged_user->id)->where('level_id', $request->input('level_id'))->where('option_id', $request->input('option_id'))->exists()){
            $level_option->level()->dissociate();
            $level_option->option()->dissociate();
            $level_option->level()->associate(Level::find($request->input('level_id')));
            $level_option->option()->associate(Option::find($request->input('option_id')));
            $level_option->save();
            return response()->json(new RESTResponse(200, "OK", null));
        }else if($level_option == null)
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez modifier n'existe pas dans la Base de données !", null));
        else
            return response()->json(new RESTResponse(300, $request->input('level_id')." : ".$request->input('option_id')." existe déjà dans votre base de données !", null));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level_option=LevelOption::find($id);
        $logged_user = Auth::user();
        if($level_option != null){
            $level_option->delete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer n'existe pas dans la Base de données !", null));
    }

    /**
     * Undelete the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function undelete($id)
    {
        $level_option=LevelOption::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($level_option != null){
            $level_option->restore();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez activer n'existe pas dans la Base de données !", null));
    }

    /**
     * Delete definitively the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $level_option=LevelOption::withTrashed()->where('id', $id);
        $logged_user = Auth::user();
        if($level_option != null){
            $level_option->forceDelete();
            return response()->json(new RESTResponse(200, "OK", null));
        }else
            return response()->json(new RESTResponse(404, "L'élément que vous souhaitez supprimer définitivement n'existe pas dans la Base de données !", null));
    }
}
