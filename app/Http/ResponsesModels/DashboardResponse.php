<?php

namespace App\Http\ResponsesModels;

class DashboardResponse
{
    public $academics_years;

    public $departments;

    public $teachers;

    public $options;

    public $hours;

    public $levels;

    public $rooms;

    public $courses;

    /**
     * Create a new DasboardResponse instance.
     *
     * @return void
     */
    public function __construct($ay, $d, $t, $o, $h, $l, $r, $c)
    {
        $this->academics_years = $ay;
        $this->departments = $d;
        $this->teachers = $t;
        $this->options = $o;
        $this->hours = $h;
        $this->levels = $l;
        $this->rooms = $r;
        $this->courses = $c;
    }
}
