<?php

namespace App\Http\ResponsesModels;

class UserResponse
{
    public $id;

    public $name;

    public $matricule;

    public $email;

    public $role;

    public $role_name;

    public $created_at;

    public $created_by;

    public $updated_at;

    public $updated_by;

    public $enabled;

    /**
     * Create a new CampagneOtherResponse instance.
     *
     * @return void
     */
    public function __construct($id, $n, $ma, $em, $rol, $roln, $ca, $ua, $enb)
    {
        $this->id = $id;
        $this->name = $n;
        $this->matricule = $ma;
        $this->email = $em;
        $this->role = $rol;
        $this->role_name = $roln;
        $this->created_at = $ca;
        $this->updated_at = $ua;
        $this->enabled = $enb;
    }
}
