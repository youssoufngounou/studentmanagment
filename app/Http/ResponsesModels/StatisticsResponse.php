<?php

namespace App\Http\ResponsesModels;

class StatisticsResponse
{
    public $children;
    public $liveBirths;
    public $target;
    public $targetRR2;
    public $TAGeneral;
    public $TAPenta;
    public $TARR;
    public $bcg;
    public $vpo0;
    public $hepB0;
    public $penta1;
    public $vpo1;
    public $pneumo131;
    public $rota1;
    public $penta2;
    public $vpo2;
    public $pneumo132;
    public $rota2;
    public $penta3;
    public $vpo3;
    public $vpi;
    public $pneumo133;
    public $vitA;
    public $rr1;
    public $vaa;
    public $rr2;
    public $menA;

    /**
     * Create a new StatisticsResponse instance.
     *
     * @return void
     */
    public function __construct($a, $b, $c, $t_RR2, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r,
                                    $s, $t, $u, $v, $w, $x, $y, $z)
    {
        $this->children = $a;
        $this->liveBirths = $b;
        $this->target = $c;
        $this->targetRR2 = $t_RR2;
        $this->TAGeneral = $d;
        $this->TAPenta = $e;
        $this->TARR = $f;
        $this->bcg = $g;
        $this->vpo0 = $h;
        $this->hepB0 = $i;
        $this->penta1 = $j;
        $this->vpo1 = $k;
        $this->pneumo131 = $l;
        $this->rota1 = $m;
        $this->penta2 = $n;
        $this->vpo2 = $o;
        $this->pneumo132 = $p;
        $this->rota2 = $q;
        $this->penta3 = $r;
        $this->vpo3 = $s;
        $this->vpi = $t;
        $this->pneumo133 = $u;
        $this->vitA = $v;
        $this->rr1 = $w;
        $this->vaa = $x;
        $this->rr2 = $y;
        $this->menA = $z;
    }
}