<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Faculty::factory()
            ->count(50)
            ->create();
        */

        DB::table('faculties')->insert([
            'name' => 'Faculté des sciense',
            'code' => "FS",
            'department_id' => 1,
            'created_at' => '2021-01-08 23:10'
        ]);
        DB::table('faculties')->insert([
            'name' => 'Faculté des sciences économiques',
            'code' => "FASEC",
            'department_id' => 1,
            'created_at' => '2021-01-08 23:10'
        ]);
    }
}
