<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrateur',
            'matricule' => '16F295S',
            'email' => 'admin@admin.cm',
            'email_verified_at' => null,
            'email_verified_at' => null,
            'password'  => bcrypt('r007@dm1n'),
            'role_id' => 1,
            'created_at' => '2020-10-23 23:10'
        ]);
    }
}
