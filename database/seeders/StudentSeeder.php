<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Student;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('students')->insertfactory('App\Models\Student', 50)->create();

        Student::factory()
            ->count(50)
            ->create();
    }
}
