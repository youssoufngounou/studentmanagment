<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            FacultySeeder::class,
            GradeSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            DepartmentSeeder::class,
            PassportSeeder::class,
            OptionSeeder::class,
            LevelSeeder::class,
            UnitSeeder::class,
            TeacherSeeder::class,
            StudentSeeder::class
        ]);
    }
}
