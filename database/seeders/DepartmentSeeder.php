<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Department::factory()
            ->count(50)
            ->create();
        */

        DB::table('departments')->insert([
            'department_code' => "MATH",
            'department_name' => 'Mathématique',
            'user_id' => 1,
            'created_at' => '2021-01-08 23:10'
        ]);
        DB::table('departments')->insert([
            'department_code' => "INF",
            'department_name' => 'Informatique',
            'user_id' => 1,
            'created_at' => '2021-01-08 23:10'
        ]);
    }
}
