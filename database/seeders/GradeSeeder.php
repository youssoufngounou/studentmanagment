<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grades')->insert([
            'title' => 'Pr. ',
            'description' => 'Professeur',
            'created_at' => '2021-01-08 23:10'
        ]);

        DB::table('grades')->insert([
            'title' => 'MC. ',
            'description' => 'Maître de Conférences',
            'created_at' => '2021-01-08 23:10'
        ]);

        DB::table('grades')->insert([
            'title' => 'Dr. ',
            'description' => 'Chargé de Cours',
            'created_at' => '2021-01-08 23:10'
        ]);

        DB::table('grades')->insert([
            'title' => 'M/Mme. ',
            'description' => 'Assistant',
            'created_at' => '2021-01-08 23:10'
        ]);
    }
}
