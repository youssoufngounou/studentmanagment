<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'description' => 'ADMINISTRATEUR',
            'created_at' => '2021-01-08 23:10'
        ]);

        DB::table('roles')->insert([
            'description' => 'DOYEN',
            'created_at' => '2021-01-08 23:10'
        ]);

        DB::table('roles')->insert([
            'description' => 'CHEF DE DEPARTEMENT',
            'created_at' => '2021-01-08 23:10'
        ]);
    }
}
