<?php

namespace Database\Factories;

use App\Models\Student;
use App\Models\Department;
use App\Models\Level;
use App\Models\Option;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'matricule' => $this->faker->unique()->username,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'department_id' => Department::factory(),
            'level_id' => Level::factory(),
            'option_id' => Option::factory(),
            'user_id' => User::factory(),
        ];
    }
}
