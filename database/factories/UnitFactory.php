<?php

namespace Database\Factories;

use App\Models\Unit;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Level;
use App\Models\Department;

class UnitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Unit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->faker->unique()->name,
            'name' => $this->faker->unique()->name,
            'description' => $this->faker->name,
            'credit_number' => $this->faker->unixTime,
            'semester' => $this->faker->unixTime,
            'department_id' => Department::factory(),
            'level_id' => Level::factory(),
            'user_id' => User::factory(),
            'teacher_id' => Teacher::factory(),
        ];
    }
}
